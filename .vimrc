colorscheme badwolf "Set colorscheme to badwolf

syntax enable "Enable syntax highlighting 

set tabstop=4 "4 visual spaces per tab

set number "Show line numbers

set showcmd "Show command in the bottom bar

set wildmenu "Visual autocomplete for command menu

set lazyredraw "Redraw only when necessary

set showmatch "Highlight matching parentheses

set incsearch "Search as characters are entered

set hlsearch "Highlight search matches

" Turn off search highlight when \space is pressed
nnoremap <leader><space> :nohlsearch<CR>

" highlight last inserted text
nnoremap gV `[v`]

set encoding=utf-8

map ; :Files<CR>

map <C-o> :NERDTreeToggle<CR>

set laststatus=2

" vim-plug
call plug#begin('~/vim_plugins')

" NERD tree will be loaded on the first invocation of NERDTreeToggle command
Plug 'scrooloose/nerdtree'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'itchyny/lightline.vim'
" Initialize plugin system
call plug#end()
