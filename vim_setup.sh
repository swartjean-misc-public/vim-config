#!/bin/bash

export TERM=xterm-256color

# Copy the .vim folder recursively to ~
rsync -a .vim ~/
cp .vimrc ~/
